function X = readpq( filename)
%X = readpq( filename);
% This function will read dat-files from the PicoQuant instrument
%
%INPUT:
% filename  Optional, if not added, a GUI will be opened where you select
%            the files you want to import
%
%OUTPUT:
% X         The imported data

%190118 AAR

if nargin == 0
    [ file, path] = uigetfile( '*.dat', 'Select PQ-files', 'MultiSelect', 'on');
    %In case only one file is selected this will only be a character array
    if ~iscell( file)
        temp = file;
        clear file
        file{1} = temp;
        clear temp
    end
else
    error( 'The function doesn''t handle a filename as input, yet')
%     if isempty( findpart( filename, '/') ) || isempty( findpart( filename, '\') )
%         path = [ cd '\'];
%         file{1} =     
end

%The extra information
info = { 'Sample_Temperature'; 'Sample_TargetTemperature'; 'Stirrer_State'; 
    'Exc_Wavelength'; 'Exc_Bandpass'; 'Exc_Polarisation'; 'Exc_Attenuation'; 
    'Det_Wavelength'; 'Det_Bandpass'; 'Det_Polarisation'; 'Det_Lens_Position'; 
    'Det_Attenuation'; 'Signal_CFD Level'; 'Signal_CFD Zero Cross'; 
    'Signal_Offset'; 'Sync_CFD Level'; 'Sync_CFD Zero Cross'; 'Sync_Offset'};

samnam = { 'Sample :'; 'Solvent :'; 'Comment :'};

g = waitbar( 0, 'Converting PicoQuant files');
for cf = 1:length( file)
    waitbar( cf/ length( file), g)
    fid = fopen( [path file{cf}], 'r');
    lab = fread( fid, Inf, 'uchar');
    fclose( fid);

    lab( lab == 13) = [];
    
    if ~isempty( findpart( char( lab( 1:1000)'), 'Sample_Temperature') )
        %Save the extra information
        for ci = 1:length( info)
            st = findpart( char( lab( 1:2000)'), [ info{ ci} ' :']) + length( info{ ci}) + 2;
            en = find( lab( st:2000) == 10, 1) + st - 2;
            temp = lab( st:en)';
            temp( temp == 47) = []; %Remove '/'
            temp( temp < 45 | temp > 57) = []; %Remove all non-numbers
            if isempty( temp)
                id( ci) = NaN;
            else
                id( ci) = str2num( char( temp) );
            end
        end
        X( cf).Info = id;
        X( cf).InfoLab = info;
        ts = { 'Start_Date : '; 'Start_Time : '};
        for ct = 1:length( ts)
            st = findpart( char( lab( 1:2000)'), ts{ ct}) + length( ts{ ct});
            en = find( lab( st:2000) == 10, 1) + st - 2;
            mt{ ct} = char( lab( st:en)');
        end
        X( cf).Time = datestr( datenum( [ mt{1} ' ' mt{2}]) );
    else
        disp( 'Please remember to NEVER save files as IRF!')
    end
    for cs = 1:length( samnam)
        st = findpart( char( lab( 1:4000)'), samnam{ cs});
        en = find( lab( st:4000) == 10, 1) + st - 2;
        sn{cs} = char( lab( st + length( samnam{ cs}) + 1:en)');
    end
    X(cf).SampleID = { [ sn{1} ' (' sn{2} ') - ' sn{3}], file{cf}};
    st = findpart( char( lab( 1:4000)'), 'Intensity [Cnts.]');
    st = find( lab( st:4000) == 10, 1) + st;
    temp = str2num( char( lab( st:end)') );
    [ X(cf).Time, X(cf).Decay] = deal( temp( :, 1), temp( :, 2));
end
close( g);

%And then try to merge those samples that have similar axes (both emission,
%excitation and time)

