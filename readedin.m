function X = readedin( file)
% Read fluorescence spectra measured on the Edinburgh, saved as txt-files
%
%INPUT:
% file  A cellstr indicating the position of the files. If not given a
%        dialogue box will pop up
%
%OUTPUT:
% X     The imported spectra, incl. axes and unfolded
%
%See also: readeem, readfl, readbv (other fluorescence instruments)

%190816 AAR

if nargin == 0
    [file,path] = uigetfile( {'*.txt'},'Files to convert', 'MultiSelect', 'on');
    %In case only one file is selected this will only be a character array
    if ~iscell( file)
        temp = file;
        clear file
        file{1} = [ path temp];
        clear temp  
    else
        file = cellstr( [ ones( length( file), 1) * path char( file')]);
    end
else
    path = '';
end

g = waitbar( 0, 'Converting Edinburgh files');
for cf = 1:length( file)
    waitbar( cf/ length( file), g)
    if ~isempty( path)
        fid = fopen( file{ cf}, 'r');
    else
        fid = fopen( [path file{cf}], 'r');
    end
    lab = fread( fid, Inf, 'uchar');
    fclose( fid);
    
    lab( lab == 10) = [];

    %I would also like to get all the settings of the instrument that is
    %stored in the txt-file
    st = findpart( char( lab'), 'Type;');
    en = find( lab( st:end) == 13, 1) + st - 2;
    temp = char( lab( st + 5:en)');
    id = findpart( temp, 'Emission');
    X(cf).InfoLab = { 'Ex Corr'; 'Ex Pol'; 'Ex Slit'; 'Em Corr'; 'Em Pol'; ...
        'Em Slit'; 'Ref Corr'; 'Dwell time'};
    if ~isempty( id) %It is Emission spectra/ EEMs
        tp = 1;
    else
        tp = 2;
        X(cf).InfoLab{ end + 1, 1} = 'Delta';
    end    
    X(cf).Info = readinfo( lab, tp);

    %Sometimes the Ex-axis has been flipped
    lr = false;
    %Find the excitation axis
    if tp == 1
        st = findpart( char( lab( 1:400)'), 'Labels');
        en = find( lab( ( st + 1):end) == 13, 1);
        temp = char( lab( st:( st + en) )');
        temp = strrep( temp, 'Ex1', '');
        num = double( temp);
        num( num < 46 | num > 57) = 32; %Set all characters except the numbers and the period as space
        num = str2num( char( num) );
        
        %Sometimes the instrument hasn't saved in the right format (!)
        if any( diff( num) < 0)
            %Sometimes the format has been flipped
            if any( diff( num) > 0)
                X(cf).Ex = NaN;
            else
                lr = true;
                X(cf).Ex = fliplr( num);
            end
        else
            X(cf).Ex = num;
        end
    else
        st = findpart( char( lab( 1:400)'), 'Start');
        en = find( lab( st( + 1):end) == 13, 1) - 3;
        temp = lab( ( st + 6):( st + en))';
        temp( temp < 46 | temp > 57) = 32;
        X(cf).Ex = str2num( char( temp));
    end
    
    %Find the actual landscape
    st = findpart( char( lab( en:end)'), 'R928;');
    st = find( lab( ( en + st(end)):( en + st(end) + 20) ) == 13, 1) + st(end) + en;
    temp = lab( st:end);
    temp = char( temp');
    %Insert -100 for the non-measured elements
    temp = double( strrep( temp, ' ;', '-100;') );
%     temp( temp == 59) = 32;

    %Don't really know why this is happening, but Nanna (141119) sent me a
    %file where a lot of the st-values was just one in between!
    test = true;
    while test
        st = find( temp == 13);        
        if any( diff( st) == 1)
            temp = double( strrep( char( temp), char( [13 13]), char( 13) ) );
        else
            test = false;
        end
    end
    
    for ce = 1:( length( st) - 1)
        num = str2num( char( temp( st( ce) + 1:( st( ce + 1) - 1) ) ) );
        %If you disturb the instrument while it saves the data, it will
        %fail to save evertyhing in the right format
        if isempty( num)
            xtemp = temp( st( ce) + 1:( st( ce + 1) - 1));
            inon = find( xtemp > 59 & xtemp ~= 69, 1);
            num = str2num( char( xtemp( 1:inon - 1)));
            if ~isempty( num)
                X(cf).Em( ce) = num(1);
                num(1) = [];
                X(cf).EEM( ce, 1:length( num)) = num;
            end
        else
            %Replace the non-measured with NaN
            num( num == -100) = NaN;
            X(cf).Em( ce) = num(1);
            X(cf).EEM( ce, :) = num( 2:end);
        end
    end
    %Sometimes the Ex-axis has been flipped
    if lr
        X(cf).EEM = fliplr( X(cf).EEM);
    end
    
    st = find( lab( 1:200) == 13, 1);
    X(cf).SampleID{1} = char( lab( 1:( st - 1) )');
    X(cf).SampleID{2} = file{cf};
    
    if tp == 2
        X(cf).Em = X(cf).Em + X(cf).Info( 9);
    end
end
close( g);

%Concatenate those landscapes that have the same size
for cs = 1:length( X)
    sz( cs, :) = size( X(cs).EEM);
end

[i, j] = fjernlike( sz);
for cj = 1:size( i, 1)
    temp = j( cj, ~isnan( j( cj, :) ) );
    for ci = 1:length( temp)
        R(cj).SampleID{ ci, 1} = X( temp( ci) ).SampleID{1};
        R(cj).SampleID{ ci, 2} = X( temp( ci) ).SampleID{2};
        %The excitation axis sometimes doesn't come out correctly
        Ex( ci, :) = X( temp( ci) ).Ex;
        R(cj).Em = X( temp( ci) ).Em;
        R(cj).EEM( ci, :, :) = X( temp( ci) ).EEM;
        R(cj).Info( ci, :) = X( temp( ci)).Info;
    end
    R( cj).InfoLab = X( temp( ci)).InfoLab;
    [o, p] = fjernlike( Ex);
    clear Ex
    R(cj).Ex = o( sum( ~isnan( o), 2) == size( o, 2), :);
end

X = R;
for cx = 1:length( X)
    try
        [ temp, emn, exn] = flucut( X(cx).EEM, X(cx).Em, X(cx).Ex, [0 NaN], [0 NaN]);
        lab = [ vec( ones( length( emn), 1) * vec( exn)') vec( ( vec( emn) * ones( 1, length( exn) ) ) )];
        tempdat = vec( squeeze( temp( 1, :, :) ));
        lab( isnan( tempdat), :) = [];
        dim = size( temp);
        temp = reshape( temp, [dim(1) prod( dim(2:3) )]);
        X(cx).Unfold = temp( :, ~isnan( tempdat) );
        X(cx).ULab = [ones( size( lab, 1), 1) * 'Ex ' num2str( lab( :, 1) ) ones( size( lab, 1), 1) * ' Em ' num2str( lab( :, 2) )];
        X(cx).UAx = lab;
    catch
        X(cx).Unfold = NaN;
        X(cx).ULab = '';
        X(cx).UAx = [];
    end
end

%--------------------------------------------------------------------------
function [indX,indY]=findpart(X,Y, num)
% [indX,indY]=findpart(X,Y, num);
%
% Searches through a character array or cellstring (X) for the
% rows including the string Y.
%
% INPUT:
%  X     The character array or cellstring
%  Y     String to search for
%  num   Only give the 'num' first occurences < default = Inf > (All)
%
% OUTPUT:
%  indX  Indeces indicating which rows that include the string
%  indY  Indeces indicating the column it starts

% 191012 AAR Don't know why I have made that extra check on line 36
% 210711 AAR In case the user only asks for one output it is set to the
%             most informative one (indX or indY depending on which is
%             longest

if nargin < 3
    num = Inf;
end

if iscell(X)
    X = strrep( X, '', ' ');
    X=char(X);
end
[r,c]=size(X);
Xvec = vec( X')';
i=strfind( Xvec, Y);
if ~isempty(i)
    indX=ceil(i/c)';
    indY=i'-(indX-1)*c;
    if nargout < 2
        if r == 1 %(length( fjernlike( indX )) == 1 && length( indY) > 1) || r == 1
            indX = indY;
        else
            indX = fjernlike( indX);
        end
    end
else
    indX=[];
    if nargout==2
        indY=[];
    end
end

if ~isinf( num) || num < length( indX)
    indX = indX( 1:num);
    indY = indY( 1:num);
end

%--------------------------------------------------------------------------
function xnew = vec( x)

xnew = x(:);

function [svar,ind]=fjernlike(x,n,lim)
%[svar,ind]=fjernlike(x,n,lim)
%
% Removes equal rows with respect to a specific column. NaN-values will not
% be counted as a unique value
%
% INPUT:
%  x      The x-matrix, can also be a string matrix
%  n      The column to check by 
%  lim    Can be added to specify a %-deviation allowed for equality
%
% OUTPUT:
%  svar   The matrix consisting of the unique values in column n
%  ind    The indexes for the equal rows

% Copyright, 2005 - 
% This M-file and the code in it belongs to the holder of the
% copyrights and is made public under the following constraints:
% It must not be changed or modified and code cannot be added.
% The file must be regarded as read-only. 
%
% �smund Rinnan
% Quality and Technology
% Department of Food Science
% Faculty of Life Sciences
% University of Copenhagen
% Rolighedsvej 30, 1958 Frederiksberg C, Denmark
% Phone:  +45 35 33 35 42
% e-mail: aar@life.ku.dk

if isempty(x)
    svar=[];
    ind=[];
    return
end
if iscell(x)
    x=char(x);
end
if nargin<3
    lim=0;
end
[r,k]=size(x);
if nargin==1
    n=1:k;
end
if isstr(x(1,:))
    [xnew,i]=sortrows(x,n);
    a=double(xnew);
    c=a(1:end-1,:)~=a(2:end,:);
    d=a(1:end-1,:)==a(2:end,:);
    %If there is only a character vector, this will also check for those
    if size(c,2)>1
        b=[1 find(sum(c')>0)+1]';
    else
        b=[1;find(c>0)+1]; 
    end
else
    [xnew,i]=sortrows(x(:,n));
    a=xnew(1:end-1,:)*(1+lim)-xnew(2:end,:)*(1-lim);
    a(a>0)=0;
    a=a.^2;
    if length(n)>1
        a=sum(a')';
    end
    b=[1;find(a~=0)+1];
end
svar=x(sort(i(b)),:);

if nargout==2
    %Find the unique rows
    a=zeros(size(x,1),1);
    a(b)=1;
    c=cumsum(a);
    %Find the equal rows
    a(a==1)=b-[0;b(1:end-1)];
    d=(1:length(a))'-cumsum(a)+1;
    [o,p]=sort(i(b));
    %Convert that into a row index with original coordinates
    ind=full(spconvert([c d i]));
    ind(ind==0)=NaN;
    %It is utterly important to include the following line!
    %If not the index of row 12 will NOT(!) be the unique row given in svar
    ind=ind(p,:);
end

%--------------------------------------------------------------------------
function info = readinfo( lab, tp)
%INPUT:
% lab The raw data
% tp  Type of instrument (1 = Em, 2 = Sync)

txt = { 'Fixed/Offset'; 'Scan '};
%Find the excitation and emission settings
for ct = 1:2
    c = ( ct - 1) * 3;
    lim = find( lab == 13, 24);
    st = findpart( char( lab( 1:lim( end))'), txt{ ct});
    en = find( lab( 1:st(end) + 1200) == 13);
    en( en < st(1)) = [];
    for cs = 1:length( st)
        temp = find( en > st( cs), 1);
        en( cs) = en( temp);
    end
    en( length( st) + 1:end) = [];
    %The first line in excitation is not interesting
    if ct == 1
        st(1) = [];
        en(1) = [];
    end
    %Correction
    temp = char( lab( st(1):en(1) - 1)');
    id = findpart( temp, 'True');
    if isempty( id)
        info( c + 1) = 0;
    else
        info( c + 1) = 1;
    end
    %Polarizer
    temp = lab( st(2):en(2) - 1)';
    stp = find( temp == double( ';'), 1);
    temp( 1:stp) = '';
    temp = strrep( char( temp), ';', ' ');
    temp = fjernlike( str2num( temp)');
    if isempty( temp)
        temp = NaN;
    end
    info( c + 2) = temp;
    %Slit
    temp = char( lab( st(3):en(3) - 1)');
    temp( 1:( 10 + ( ct == 1) * 7)) = '';
    temp = strrep( temp, ';', ' ');
    temp = fjernlike( str2num( temp)');
    info( c + 3) = temp;
end

%Correction by reference
st = findpart( char( lab( 1:lim(end))'), 'Corr. by Ref. Det.');
en = find( lab( st:lim(end)) == 13, 1) + st - 2;
temp = char( lab( st:en)');
id = findpart( temp, 'True');
if isempty( id)
    info( 7) = 0;
else
    info( 7) = 1;
end

%Dwell time
st = findpart( char( lab( 1:lim(end))'), 'Dwell Time');
en = find( lab( st:lim(end)) == 13, 1) + st - 2;
temp = char( lab( st:en)');
temp( 1:11) = '';
temp = strrep( temp, ';', ' ');
info( 8) = fjernlike( str2num( temp)');

%Delta for syncronous
if tp == 2
    st = findpart( char( lab( 1:lim(end))'), 'Fixed/Offset;');
    en = find( lab( st:lim( end)) == 13, 1) + st - 2;
    temp = char( lab( st:en)');
    temp( 1:13) = '';
    temp = strrep( temp, ';', ' ');
    info( 9) = fjernlike( str2num( temp)');
end