function X = readkin( file)
% X = readkin( file)
% Reads kinetic scans from the Edinburgh fluorimeter
%
%INPUT:
% file   A string with file name or cell with file names. If omitted, a
%         dialogue box pops up
%
%OUTPUT:
% X      A struct with the imported data
%
%See also: readedin

%AAR 221025

if nargin == 0
    [ file, path] = uigetfile( { '*.txt', 'Files from Edinburgh'}, 'Select files to import', 'MultiSelect', 'on');    
else
    if ischar( file)
        file = cellstr( file);
    end
    path = cd;
end

for cf = 1:length( file)
    fid = fopen( [ path '\' file{cf}], 'r');
    lab = fread( fid, inf, 'uchar');
    fclose( fid);
    lab( lab == 10) = [];
    X.SampleID{ cf, 1} = file{cf};
    st = find( lab == 13);
    txt = char( lab( 1:st(1)-1)');
    txt = strrep( txt, 'Labels;', '');
    txt = strrep( txt, ';', '');
    X.SampleID{ cf, 2} = txt;
    %In order to get the time
    d = dir( [ path '\' file{cf}]);
    X.Date{ cf, 1} = datestr( d.datenum);
    %Extract the data
    stc = findpart( char( lab( 1:st( 12))'), 'Counts');
    st = st( find( st > stc, 10) );
    ds = diff( st);
    st = st( find( ds > 2, 1)) + 1;
    
    temp = lab( st:end)';
    temp( temp == 59) = 32;
    X.Data{ cf} = str2num( char( temp));
end

%--------------------------------------
function [indX,indY]=findpart(X,Y, num)
% [indX,indY]=findpart(X,Y, num);
%
% Searches through a character array or cellstring (X) for the
% rows including the string Y.
%
% INPUT:
%  X     The character array or cellstring
%  Y     String to search for
%  num   Only give the 'num' first occurences < default = Inf > (All)
%
% OUTPUT:
%  indX  Indeces indicating which rows that include the string
%  indY  Indeces indicating the column it starts

% 191012 AAR Don't know why I have made that extra check on line 36
% 210711 AAR In case the user only asks for one output it is set to the
%             most informative one (indX or indY depending on which is
%             longest

if nargin < 3
    num = Inf;
end

if iscell(X)
    try
        X = strrep( X, '', ' ');
    catch %There are some numbers in the cellstr these need to be converted
        xr = size( X, 1);
        for cr = 1:xr
            if isnumeric( X{ cr, 1})
                X{ cr, 1} = num2str( X{ cr, 1});
            end
        end
    end
    X=char(X);
end
[r,c]=size(X);
Xvec = vec( X')';
i=strfind( Xvec, Y);
if ~isempty(i)
    indX=ceil(i/c)';
    indY=i'-(indX-1)*c;
    if nargout < 2
        if r == 1 %(length( fjernlike( indX )) == 1 && length( indY) > 1) || r == 1
            indX = indY;
        else
            indX = fjernlike( indX);
        end
    end
else
    indX=[];
    if nargout==2
        indY=[];
    end
end

if ~isinf( num) || num < length( indX)
    indX = indX( 1:num);
    indY = indY( 1:num);
end

function xnew = vec( x)

xnew = x(:);