function data=readeem( file, path)
%data=readeem( file, path);
%
% Read csv-files from Varian using the ADL to record spectra
%
% INPUT:
%  file  Optional
%  path  Optional
%
% OUTPUT:
%  data  In a struct array with Ex, Em, Lab and EEM

% 180506 AAR

%Only search for the first excitation

if nargin<1 || isempty(file) || nargin<2 || isempty(path) %"if + input" added DPA 06-08-2016
   [file,path]=uigetfile( {'*_0.csv'},'Files to convert', 'MultiSelect', 'on');
end

if ~iscell( file)
    file = cellstr( file);
end

%Make sure that all files have the same number of ex
m = zeros( length( file), 1);
for i=1:length(file)
    id = findpart( file{i}, '_');
    d=dir([path file{i}(1:id(end)) '*csv']);
%     temp = double( char( d.name) );
    m(i)=length(d);
end
[m, n] = fjernlike(m);
if length(m)>1
    for cm = 1:length( m)
        fprintf( [ num2str( cm) ': ' num2str( [m(cm) sum( ~isnan( n( cm, :) ) )]) char( 10)] )
    end
    test = true;
    s = lower( input( 'Would you like to continue with one of these groups (Y/N)? ', 's') );
    if strcmp( s, 'y')
        s = input( 'What group would you like to convert? ');
        if s <= length( m) && s > 0
            m = m( s);
            file = file( n( s, ~isnan( n( s, :) ) ) );
            test = false;
        end
    end
    if test
        error('All spectra must have the same number of excitations')
    end
end
m = m - 1; %In order for the code to be correct further down

%Start reading the files and putting them into Matlab format
waitb=waitbar(0,'Conversion in progress');
for i=1:length(file)
    waitbar(i/length(file),waitb);
    id = findpart( file{i}, '_');
    count=1;
    %Instead of checking exactly what emission and exitations measured, all
    %the data are put into a sparse matrix
    
    %Read all the files that makes up one sample
    for j=0:m
        if j==0
            temp=dir([path file{i}(1:id(end)) num2str(j) '.csv']);
            data.Date{i,1}=temp.date;
        end
        fid=fopen([path file{i}(1:id(end)) num2str(j) '.csv'],'r');
        lab=fread(fid,inf,'uchar');
        fclose(fid); %Close the file as soon as it has been read
        
        lab(lab==13)=[]; %Remove all 'Enter'
        
        %In case ';' has been used instead of ','
        lab( lab == 59) = 44;
        
        %In case the file is from the old "general_setup.adl" file
        exid=findstr(char(lab'),'EX');
        
        if ~isempty( exid) %Old format
            lab(exid(2):end)=[]; %Remove part of the text-file which does not hold any spectral information
            pos=find(lab==10);
            temp=char(lab(exid:pos(1))');
            st=findstr(temp,'EX_');
            en=findstr(temp,',,');
            Ex=str2num(temp(st+3:en-1));
            
            %Find emission wavelengths and absorption according to file
            for k=1:length(pos)-2
                temp=lab(pos(k)+1:pos(k+1)-1);
                if temp(1)>47 && temp(1)<58
                    ci=find(temp==44);
                    em(count)=round(str2num(char(temp(1:ci(1)-1)')));
                    ab(count)=str2num(char(temp(ci(1)+1:ci(2)-1)'));
                    ex(count)=Ex;
                    count=count+1;
                end
            end
        else %New format
            %First need to find the excitation wavelenth
            en = find( lab == 10, 1);
            ex( count) = str2num( char( lab( 1:en-1)') );
            temp = str2num( char( lab( en + 1:end)') );
            if i == 1 && j == 0
                % The instrument is a bit "funny" with this axis. This code
                % is "just" to make it uniform
                em = temp( :, 1);
                em = linspace( em( 1), em( end), length( em) );
               
                ab( :, count) = temp( :, 2);
            else
                %If the emission axis changes
                if size( temp, 1) < length( em) && temp( 1, 1) == em(1) && temp( end, 1) == em(end)
                    %191216 AAR I have obviously done something wrong when
                    %            I write the '-1' entries into the files
                    temp( temp( :, 2) == -1, :) = [];
                    %191216 AAR I also correct the emission so that they
                    %            don't become "weird" numbers
                    temp( 1, 1) = round( temp( 1, 1) * 2)/ 2;
                    
                    emt = linspace( temp( 1, 1), temp( end, 1), size( temp, 1) );
                    [o, p] = fjernlike( [em emt]'); %Find the part of the axis that has been measured   
                    try
                        ab( p( ~isnan( p( :, 2) ), 1), count) = temp( :, 2);
                        ab( p( isnan( p( :, 2) ), 1), count) = NaN;
                    catch
                        error( 'It seems that your emission spectra do not have the same length')
                    end
                    
                    %131216 AAR Not a perfect fit, but anyhow
                elseif size( temp, 1) ~= length( em)                    
                    if size( temp, 1) > length( em)
                        em = temp( :, 1);
                        em = linspace( em( 1), em( end), length( em) );
                    end
                    diffem = fjernlike( diff( em(:)));
                    [ o, st] = min( abs( em - temp( 1, 1) ) );
                    if o >= diffem
                        [q, en] = min( abs( em - temp( end, 1) ) );
                        st = en - size( temp, 1) + 1;
                        en = w;
                    else
                        en = st + size( temp, 1) - 1;
                    end
%                     if st < 1
                    ab( st:en, count) = temp( :, 2);
                    ab( ab( :, count) == 0, count) = NaN;                    
                    
                else %If the emission axis stays the same
                    ab( :, count) = temp( :, 2);
                end
            end
            count = count + 1;
        end
    end    
    
    %This is what I set the missing values in Varian to
    ab( ab == -1) = NaN;
    
    %Enter all the information into a sparse matrix, and remove the rows
    %and columns with only 0's
    if ~isempty( exid) %Old format
        xtemp=sparse(em,ex,ab);
    else %New format
        xtemp = ab;        
%         xtemp = fliplr( ab);
    end
    ir=find(sum(xtemp, 2)~=0);
    ic=find(sum(xtemp)~=0)';
    if i>1
        if length(ir)~=length(data.Em)
            if length(ir)>length(data.Em)
            else
                ir=data.Em;
            end
        end
        if length(ic)~=length(data.Ex)
            if length(ic)>length(data.Em)
            else
                ic=data.Ex;
            end
        end
    end
    if length(ir)>size(xtemp,1) || length(ic)>size(xtemp,2)
        error('Your EEM''s have different size')
    end
    xtemp=xtemp(ir,ic);
    xtemp=full(xtemp);
%     xtemp(xtemp==0)=NaN; %Insert NaN's instead of 0's in all other places
    if i==1
        data.Ex=sort(fjernlike(ex'));
        data.Em=sort(fjernlike(em'));
        dim=size(xtemp);
    end
    data.SampleID{i,1}=file{i}(1:end-6);
    
    %AAR 250214 Get an error when the size is not the same
    if i > 1
        if size( xtemp, 1) > size( data.EEM, 2)
            xtemp( end, :) = [];
        end
    end
    data.EEM(i,:,:)=xtemp;
    clear Xab
end

if length( fjernlike( round( data.Em) ) ) == length( data.Em)
    data.Em = round( data.Em);
end

for i = 1:size( data.EEM, 1)
    data.Unfold( i, :) = vec( data.EEM( i, :, :) )';
end
temp = vec( data.Em (:) * ones( 1, length( data.Ex) ) );
temp( :, 2) = vec( ones( length( data.Em), 1) * data.Ex(:)');
data.ULab = [ones( size( temp, 1), 1) * 'Em' num2str( temp( :, 1) ) ...
    ones( size( temp, 1), 1) * '_Ex' num2str( temp( :, 2) )];
id = isnan( data.Unfold( 1, :) );
data.Unfold( :, id) = [];
data.ULab( id, :) = [];
close(waitb)

%--------------------------------------------------------------------------
function [svar,ind]=fjernlike(x,n,lim)
%[svar,ind]=fjernlike(x,n,lim)
%
% Removes equal rows with respect to a specific column. NaN-values will not
% be counted as a unique value
%
% INPUT:
%  x      The x-matrix, can also be a string matrix
%  n      The column to check by 
%  lim    Can be added to specify a %-deviation allowed for equality
%
% OUTPUT:
%  svar   The matrix consisting of the unique values in column n
%  ind    The indexes for the equal rows

% Copyright, 2005 - 
% This M-file and the code in it belongs to the holder of the
% copyrights and is made public under the following constraints:
% It must not be changed or modified and code cannot be added.
% The file must be regarded as read-only. 
%
% �smund Rinnan
% Quality and Technology
% Department of Food Science
% Faculty of Life Sciences
% University of Copenhagen
% Rolighedsvej 30, 1958 Frederiksberg C, Denmark
% Phone:  +45 35 33 35 42
% e-mail: aar@life.ku.dk

% AAR 140507 Now also handles character vectors
% AAR 271006 Gives now the correct ind, so that x(ind(12,:),:)=svar(12,:)
% AAR 300605 Made the algorithm notably faster for both numbers and
%             strings.
% AAR 290605 Made the algorithm faster by checking for the length of the
%             unique and similar components. (Can mean a lot if the number
%             of rows are high!) (Not implemented for strings)
% AAR 290605 Change cell to char
% AAR 170105 Changed if 'x' is string to use b~=
% AAR 130105 Changed 'ind' so that it is not made unnecessarily large

if isempty(x)
    svar=[];
    ind=[];
    return
end
if iscell(x)
    x=char(x);
end
if nargin<3
    lim=0;
end
[r,k]=size(x);
if nargin==1
    n=1:k;
end
if isstr(x(1,:))
    [xnew,i]=sortrows(x,n);
    a=double(xnew);
    c=a(1:end-1,:)~=a(2:end,:);
    d=a(1:end-1,:)==a(2:end,:);
    %If there is only a character vector, this will also check for those
    if size(c,2)>1
        b=[1 find(sum(c')>0)+1]';
    else
        b=[1;find(c>0)+1]; 
    end
else
    [xnew,i]=sortrows(x(:,n));
    a=xnew(1:end-1,:)*(1+lim)-xnew(2:end,:)*(1-lim);
    a(a>0)=0;
    a=a.^2;
    if length(n)>1
        a=sum(a')';
    end
    b=[1;find(a~=0)+1];
end
svar=x(sort(i(b)),:);
if ~isstr(x(1,:))
    if size(svar,2)==1
        svar=svar(~isnan(svar));
    else
        svar=svar(sum(~isnan(svar)')==size(svar,2),:);
    end
end

if nargout==2
    %Find the unique rows
    a=zeros(size(x,1),1);
    a(b)=1;
    c=cumsum(a);
    %Find the equal rows
    a(a==1)=b-[0;b(1:end-1)];
    d=(1:length(a))'-cumsum(a)+1;
    [o,p]=sort(i(b));
    %Convert that into a row index with original coordinates
    ind=full(spconvert([c d i]));
    ind(ind==0)=NaN;
    %It is utterly important to include the following line!
    %If not the index of row 12 will NOT(!) be the unique row given in svar
    ind=ind(p,:);
end

%--------------------------------------------------------------------------
function xnew=vec(x)
%xnew=vec(x)
%
% Vectorices the matrix x

xnew=x(:);

%--------------------------------------------------------------------------
function [indX,indY]=findpart(X,Y, num)
% [indX,indY]=findpart(X,Y, num);
%
% Searches through a character array or cellstring (X) for the
% rows including the string Y.
%
% INPUT:
%  X     The character array or cellstring
%  Y     String to search for
%  num   Only give the 'num' first occurences < default = Inf > (All)
%
% OUTPUT:
%  indX  Indeces indicating which rows that include the string
%  indY  Indeces indicating the column it starts

% 191012 AAR Don't know why I have made that extra check on line 36
% 210711 AAR In case the user only asks for one output it is set to the
%             most informative one (indX or indY depending on which is
%             longest

if nargin < 3
    num = Inf;
end

if iscell(X)
    X = strrep( X, '', ' ');
    X=char(X);
end
[r,c]=size(X);
Xvec = vec( X')';
i=strfind( Xvec, Y);
if ~isempty(i)
    indX=ceil(i/c)';
    indY=i'-(indX-1)*c;
    if nargout < 2
        if r == 1 %(length( fjernlike( indX )) == 1 && length( indY) > 1) || r == 1
            indX = indY;
        else
            indX = fjernlike( indX);
        end
    end
else
    indX=[];
    if nargout==2
        indY=[];
    end
end

if ~isinf( num) || num < length( indX)
    indX = indX( 1:num);
    indY = indY( 1:num);
end